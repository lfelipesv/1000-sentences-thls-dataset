# 1000-sentences-thls-dataset

THLS Open Source Brazilian Portuguese Speech Dataset with 1000 sentences balanced phonetically.

/Sentences: folder where the .wav files are located

sentences.txt: contains the transcription of each sentence

ciriglianoetal2005: paper where the dataset was proposed.

## Authors

- Luiz Felipe Vecchietti
- Thalles Melo Batista Pieroni (Voice)

## How to cite this dataset

Vecchietti, L. F. (2023). THLS - An open source dataset for Brazilian Portuguese speech processing [Data set]. Zenodo. https://doi.org/10.5281/zenodo.10416917